# 暂时不公开主题源码了，如有需要可加QQ私聊~

## 主题名称：唤醒-hankin
原主题```Angulr - Bootstrap Admin``` 的开发，本主题是```wordpress```版本

作者网站：<a href="https://www.hankin.cn">www.hankin.cn</a>

> pjax无刷新体验

> 14种配色，5种布局

> 灯箱、播放器等部件

> 最强大的后台设置

> 丰富的自定义页面

QQ群：21310971

## 主题简介
<a data-fancybox="gallery" no-pjax="" alt="主题简介" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/05/111.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/05/111.png" style="border:none"></a>
### 主题名称：唤醒-hankin
原主题```Angulr - Bootstrap Admin``` 的开发，本主题是```wordpress```版本

------------

### 主题设置包括：
#### 控制台
<a data-fancybox="gallery" no-pjax="" alt="控制台" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-01.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-01.png" style="border:none"></a>
#### 初级设置
> 头像

> icon设置

> logo设置

> 昵称

> 网站备案号

> 自定义底部

> 网站公告栏

> 简介

> 自定义社交链接

<a data-fancybox="gallery" no-pjax="" alt="初级设置" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-02.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-02.png" style="border:none"></a>
#### 高级设置
> 开启付款二维码

> 网站维护设置

> 网站后台登录背景设置

> 使用markdown编辑器

> 前端登录

> 前端源码Gzip压缩

> 全站无刷新Pjax

<a data-fancybox="gallery" no-pjax="" alt="高级设置" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-03.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-03.png" style="border:none"></a>
#### 页面元素
> 显示侧边抽屉按钮

> 显示博主信息按钮

> 显示博主信息

> 显示导航

> 显示组成（分类）

> 显示组成（页面）

<a data-fancybox="gallery" no-pjax="" alt="页面元素" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-04.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-04.png" style="border:none"></a>
#### 外观设置
> 开启背景

> body背景

> 显示换肤按钮

> 主题风格(有颜色区块选择更加明显方便)

> 固定头部

> 固定导航

> 折叠导航

> 置顶导航

> 盒子模型

<a data-fancybox="gallery" no-pjax="" alt="外观设置" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-05.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-05.png" style="border:none"></a>
#### 文章设置
> 开启文章形式模式

> 文章默认封面使用微软Bing封面（可以随机不重复显示）

> 文章页打赏作者（微信）

> 文章页打赏作者（支付宝）

<a data-fancybox="gallery" no-pjax="" alt="文章设置" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-06.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-06.png" style="border:none"></a>
#### 评论设置
> 显示UserAgent归属地信息

> 启用SMPT功能

> 启用邮件提醒

> 评论审核通知用户

> 评论回复通知用户

> 网站后台登陆失败通知管理员

> 注册用户资料信息更新通知用户

<a data-fancybox="gallery" no-pjax="" alt="评论设置" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-07.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-07.png" style="border:none"></a>
#### 音乐(仅支持网易云音乐)
> 开启播放器

> 开启自动播放

> 显示播放列表

> 音乐循环

> 音乐播放列表id

<a data-fancybox="gallery" no-pjax="" alt="音乐" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-08.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-08.png" style="border:none"></a>
#### 幻灯片
> 首页开启幻灯片

> 自定义幻灯片

<a data-fancybox="gallery" no-pjax="" alt="幻灯片" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-09.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-09.png" style="border:none"></a>
#### 友情链接
> 自定义友情链接

<a data-fancybox="gallery" no-pjax="" alt="友情链接" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-10.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-10.png" style="border:none"></a>
#### SEO
> 关键词

> 描述

<a data-fancybox="gallery" no-pjax="" alt="SEO" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-11.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-11.png" style="border:none"></a>
#### 自定义代码
> 自定义css

> 自定义javascript

> 统计代码

<a data-fancybox="gallery" no-pjax="" alt="自定义代码" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-12.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-12.png" style="border:none"></a>
#### 备份
<a data-fancybox="gallery" no-pjax="" alt="自定义代码" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/05/hankin13.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-13.png" style="border:none"></a>
#### 关于唤醒
<a data-fancybox="gallery" no-pjax="" alt="自定义代码" data-type="image" href="https://www.hankin.cn/wp-content/uploads/2018/05/hankin14.png" class="light-link"><img src="https://www.hankin.cn/wp-content/uploads/2018/06/hankin-14.png" style="border:none"></a>
#### 打赏作者
<a data-fancybox="gallery" no-pjax="" alt="微信" data-type="image" href="https://www.hankin.cn/wp-content/themes/hankin/images/dashang_weixin.png" class="light-link">
<img src="https://www.hankin.cn/wp-content/themes/hankin/images/dashang_weixin.png" 
style="border:none;width:300px;height:300px;"></a>
